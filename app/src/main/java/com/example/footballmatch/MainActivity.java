package com.example.footballmatch;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {
    private Integer counter_home = 0;
    private Integer counter_enemy = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClickGoalsHome(View view) {
        counter_home++;
        TextView counterView = findViewById(R.id.goalsHome);
        counterView.setText(counter_home.toString());
    }
    public void onClickGoalsEnemy(View view){
        counter_enemy++;
        TextView counterView = findViewById(R.id.goalsEnemy);
        counterView.setText(counter_enemy.toString());
    }

    public void onClickStop (View view){
        counter_enemy=0;
        counter_home=0;
        TextView counterViewOne = findViewById(R.id.goalsHome);
        counterViewOne.setText(counter_home.toString());
        TextView counterViewTwo = findViewById(R.id.goalsHome);
        counterViewTwo.setText(counter_home.toString());
    }


    // Вызывается перед выходом из активного состояния,
    // позволяя сохранить состояние в объекте savedInstanceState
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Объект savedInstanceState будет в последующем
        // передан методам onCreate и onRestoreInstanceState
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putInt(String.valueOf(counter_home), counter_enemy);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if(savedInstanceState.containsKey("counter_home")){
            counter_home=savedInstanceState.getInt("counter_home");
        }
        if(savedInstanceState.containsKey("counter_enemy")) {
            counter_enemy = savedInstanceState.getInt("counter_enemy");
        }
            // Восстановление состояние UI из объекта savedInstanceState.
        // Данный объект также был передан методу onCreate.
    }




}